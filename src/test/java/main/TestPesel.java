package main;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestPesel {

    private PeselValidator pesel1;
    private PeselValidator pesel2;
    private PeselValidator pesel3;
    private PeselValidator pesel4;
    private PeselValidator pesel5;
    private PeselValidator pesel6;
    private PeselValidator pesel7;
    private PeselValidator pesel8;
    private PeselValidator pesel9;



    @BeforeEach
    public void init() throws ErrorPesel {
        pesel1 = new PeselValidator("90820268311");
        pesel2 = new PeselValidator("56810169716");
        pesel3 = new PeselValidator("56231589218");
        pesel4 = new PeselValidator("33523147916");
        pesel5 = new PeselValidator("22660618412");
        pesel6 = new PeselValidator("99123118714");
        pesel7 = new PeselValidator("00810164417");
        pesel8 = new PeselValidator("99723176718");
        pesel9 = new PeselValidator("90680111718");
    }

    @Test
    public void corretPesel1() {
        assertTrue(pesel1.getDateOfBirth().equals("02.02.1890"));
    }
    @Test
    public void corretPesel2() {
        assertTrue(pesel2.getDateOfBirth().equals("01.01.1856"));
    }
    @Test
    public void corretPesel3() {
        assertTrue(pesel3.getDateOfBirth().equals("15.03.2056"));
    }
    @Test
    public void corretPesel4() {
        assertTrue(pesel4.getDateOfBirth().equals("31.12.2133"));
    }
    @Test
    public void corretPesel5() {
        assertTrue(pesel5.getDateOfBirth().equals("06.06.2222"));
    }
    @Test
    public void corretPesel6() {
        assertTrue(pesel6.getDateOfBirth().equals("31.12.1999"));
    }
    @Test
    public void corretPesel7() {
        assertTrue(pesel7.getDateOfBirth().equals("01.01.1800"));
    }
    @Test
    public void corretPesel8() {
        assertTrue(pesel8.getDateOfBirth().equals("31.12.2299"));
    }
    @Test
    public void corretPesel9() {
        assertTrue(pesel9.getDateOfBirth().equals("01.08.2290"));
    }






}
